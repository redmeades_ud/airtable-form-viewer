<script src="<?php echo AFV_PLUGIN_URL; ?>assets/js/lib/vue.js"></script>
<script src="<?php echo AFV_PLUGIN_URL; ?>assets/js/lib/axios.min.js"></script>
<link rel="stylesheet" href="<?php echo AFV_PLUGIN_URL; ?>assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo AFV_PLUGIN_URL; ?>assets/css/vue-component.css"/>

<style>
[v-cloak] {
  display: none;
}
</style>

<div v-cloak id="airtable-form-review">

    <span v-if="loading" class="spinner-border" role="status"></span>
    <span v-if="loading">{{ loading }}</span>

    <div v-if="error">
        <h3><img src='<?php echo AFV_PLUGIN_URL; ?>assets/img/x-mark-64.png'/>An error has occurred.</h3>
        <p>{{ error }}</p>
        <p>Please contact your system administrator for help.</p>
    </div>

    <div v-if="response">
        
        <p>
            <h3 v-if="response.error">
                <img src='<?php echo AFV_PLUGIN_URL; ?>assets/img/x-mark-64.png'/>
                An error occurred while recording your response.
            </h3>

            <h3 v-else-if="response.queued">
                <img src='<?php echo AFV_PLUGIN_URL; ?>assets/img/checkmark-64.png'/>
                Your response has been queued for processing.
            </h3>

            <h3 v-else>
                <img src='<?php echo AFV_PLUGIN_URL; ?>assets/img/checkmark-64.png'/>
                Your response has been recorded.
            </h3>

        </p>

        <table>
            <tr>
                <th>Form Name</th>
                <td>{{ response.formName }}</td>
            </tr>
            <tr>
                <th>Workflow Sequence</th>
                <td>{{ response.sequence }}</td>
            </tr>
            <tr>
                <th>Result</th>
                <td>{{ response.resultText }}</td>
            </tr>
        </table>

        <p>You can close this window now.</p>

    </div>

    <div v-if="should_show_app">

        <table v-if="should_show_form" class="afv-form-table">
            <tr>
                <th>Form Name</th>
                <td style="background-color:white">{{ input.formName }}</td>
            </tr>
            <tr v-for="field in form" :key='field.name'>
                <th>{{ field.name }}</th>
                <td style="background-color:white">{{ field.text }}</td>
            </tr>
        </table>

        <div id="afv_form_actions">
            <p>
                <h4>Response</h4>
                <p>Please provide a brief explanation of your response. (This message will be seen by the applicant.)</p>
                <textarea v-model="reason" rows='4'></textarea>
            </p>

            <p v-if="validation_messages.length">
                <b>Please correct the following error(s):</b>
                <ul>
                    <li v-for="message in validation_messages">{{ message }}</li>
                </ul>
            </p>

            <p v-if="!new_status">
                <button v-on:click="set_status" class='afv-button afv-button-approved' data-status='APPROVED'>Approve</button>&nbsp;
                <button v-on:click="set_status" class='afv-button afv-button-queried' data-status='QUERIED'>Query</button>&nbsp;
                <button v-on:click="set_status" class='afv-button afv-button-denied' data-status='DENIED'>Deny</button>&nbsp;
            </p>
        </div>

        <transition name="fade">
            <div v-if="new_status" id="afv_confirm">

                <div v-if="new_status=='APPROVED'">
                    <h4>You have chosen to APPROVE this application.</h4>
                    <p>The applicant will be notified, along with your explanation entered above.</p>
                    <p>If you are ready to confirm your response, please click 'Confirm' below:</p>
                </div>

                <div v-if="new_status=='QUERIED'">
                    <h4>You have chosen to ask the applicant for more information.</h4>
                    <p>
                        The applicant will be sent an email with the questions you entered above.
                        When they have ammended their application, you will be sent another email
                        asking you to review their application again. 
                    </p>
                    <p>If you are ready to confirm your response, please click 'Confirm' below:</p>
                </div>

                <div v-if="new_status=='DENIED'">
                    <h4>You have chosen to DENY this application.</h4>
                    <p>The applicant will be notified, along with your explanation entered above.</p>
                    <p>If you are ready to confirm your response, please click 'Confirm' below:</p>
                </div>

                <p>
                    <button v-on:click="confirm_status" class="afv-button afv-button-confirm">CONFIRM</button>&nbsp;
                    <button v-on:click="cancel_status" class="afv-button afv-button-cancel" >Back to Form</button>&nbsp;
                </p>

            </div>
        </transition>

    </div>

</div>

<script>
var app = new Vue({
  
    el: '#airtable-form-review',
  
    data: {
        loading: null,
        error: null,
        validation_messages: [],
        nonce: '<?php echo $nonce; ?>',
        admin_url: '<?php echo admin_url('admin-ajax.php'); ?>',
        input: JSON.parse('<?php echo $input_json; ?>'),
        new_status: null,
        reason: null,
        form: null,
        response: null
    },
  
    mounted: function() {
        console.log('input');
        console.log(this.input);
        this.load_form();
    },

    computed: {
        
        should_show_form: function() {
            return this.form && !this.new_status && !this.response;
        },

        should_show_app: function() {
            return !this.loading && !this.error && !this.response;
        },

    },

    methods: {

        set_status: function (event) {
            if (this.validate()) {
                this.new_status = event.target.dataset.status;
            }
        },

        validate: function() {
            this.validation_messages = [];
            if (!this.reason) {
                this.validation_messages.push('Please enter a reason for your decision.');
            }
            return this.validation_messages.length == 0;
        },

        confirm_status: function() {
            if (this.validate()) {
                this.send_response();
            } 
        },

        cancel_status: function() {
            this.new_status = '';
        },

        load_form: function() {

            this.loading = 'Retrieving form...';

            params = this.input;
            params.action = 'afv_load_form_ajax';
            params._ajax_nonce = this.nonce;

            console.log('load_form:request');
            console.log(params);

            axios.get(this.admin_url, {params: params})
            .then(response => {

                console.log('load_form:response');
                console.log(response);
                
                if (response.data.data.error) {
                    this.error = response.data.data.error;
                } else {
                    this.form = response.data.data.form;
                }
            })
            .catch(error => {
                console.log(error);
                this.error = error;
            })
            .then(() => this.loading = false);

        },

        send_response: function() {

            this.loading = 'Sending your response...';

            params = this.input;
            params.action = 'afv_save_response_ajax';
            params._ajax_nonce = this.nonce;
            params.status = this.new_status;
            params.reason = this.reason;

            console.log('send_response:request');
            console.log(params);

            axios.get(this.admin_url, {params: params})
            .then(response => {
                
                console.log('send_response:response');
                console.log(response);

                if (response.data.data.error) {
                    this.error = response.data.data.error;
                } else {
                    this.response = JSON.parse(response.data.data.response.body);
                    this.response.error = this.response.result == 'ERROR';
                    this.response.queued = this.response.result == 'QUEUED';
                }
            })
            .catch(error => {
                this.error = error;
            })
            .then(() => this.loading = false);

        }
  }

})
</script>