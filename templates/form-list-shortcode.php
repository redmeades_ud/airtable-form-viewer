<ul class='afv-form-list'>

<?php foreach ($forms as $form): ?> 

    <li class='afv-form-list-item'>
        <a class='afv-form-list-link' href='<?php echo get_permalink($form); ?>'>
            <?php echo $form->post_title; ?>
        </a>
    </li>

<?php endforeach; ?>

</ul>

