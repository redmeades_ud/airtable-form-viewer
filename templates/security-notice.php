<img src='<?php echo AFV_PLUGIN_URL; ?>assets/img/lock.png'/>
<h3>Security Notice</h3>
<p><b>This form can only be accessed by clicking the secure link in your email.</b></p>
<p>Please find the original email you were sent, and open the form from there.</p>
<p>If you need help with this, please contact your system administrator.</p>