<h2>Login Required</h2>

<p>You must log in to view this form</p>

<p><a href='<?php echo wp_login_url( get_the_permalink(), true ); ?>'>Click here</a> to login now.</p>