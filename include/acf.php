<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define('ACF_DIR', AFV_PLUGIN_DIR . 'lib/acf-pro/' );
define('ACF_URL', AFV_PLUGIN_URL . 'lib/acf-pro/' );
define('ACF_JSON', AFV_PLUGIN_DIR . 'acf-json/' );
define('ACF_SHOW_ADMIN', true);

if (!class_exists('ACF')) {
	add_filter('acf/settings/path', function ($path) { return ACF_URL; });
	add_filter('acf/settings/dir', function ($path) { return ACF_DIR; });
	add_filter('acf/settings/load_json', function($paths) { return ACF_JSON; });
	add_filter('acf/settings/save_json', function($paths) { return ACF_JSON; });
	add_filter('acf/settings/show_admin', function($show) { return ACF_SHOW_ADMIN; });
    include_once(ACF_DIR . 'acf.php');
}

if (function_exists('acf_add_options_page')) {
	acf_add_options_page([
		'page_title' => 'Airtable Form Settings',
		'menu_title' => 'Airtable Forms',
		'capability' => 'manage_options',
		'autoload' => true,
	]);
}

add_filter('acf/load_field/name=forms_base', 'afv_load_base_select_options');
add_filter('acf/load_field/name=authentication_base', 'afv_load_base_select_options');
function afv_load_base_select_options($field) {
	$field['choices'] = airtable_load_base_list();
	return $field;
}