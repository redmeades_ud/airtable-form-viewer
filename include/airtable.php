<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define('AFV_AIRTABLE_FORMAT_STRING', '?cellFormat=string&timeZone=Australia/Melbourne&userLocale=en-au');

/**
 * Load from the Workflow Form List table.
 */
function airtable_load_form_settings($form_name) {
	$settings = AirtableSettings::load();
	return airtable_find_single_record_by_field($settings->forms_base, 'Workflow Form List', 'Form Name', $form_name, 'fields');
}

/**
 * 
 */
function airtable_copy_record($source_base, $source_table, $source_record_id, $target_base, $target_table) {

	//load source record
	$result = airtable_load_record($source_base, $source_table, $source_record_id);
	$source_record = $result['fields'];

	//get field list for target table
	$schema = airtable_load_table_schema($target_base, $target_table);

	//look for shared editable fields
	$payload = array_filter($source_record, function($field_name) use($schema) {
		$writeable = airtable_is_field_writeable($schema[$field_name]['type']) && 
		$exists = array_key_exists($field_name, $schema);
		return $exists && $writeable;
	}, ARRAY_FILTER_USE_KEY);

	//is there a contact to look up?

	//TODO: change to link via Staff ID
	if (array_key_exists('Contact', $schema) && array_key_exists('Email Address', $source_record)) {
		$search_fields = ['Primary Email', 'Secondary Email', 'Alternate Email'];
		if ($contact_id = airtable_find_single_record_by_field($target_base, 'Contacts', $search_fields, $source_record['Email Address'])) {
			$payload['Contact'] = $contact_id;
		}
	}

	//apply to target table
	return airtable_update_record($target_base, $target_table, $payload);

}

/**
 * 
 */
function airtable_load_record($base_id, $table_name, $record_id, $raw = false) {
	$source_url = join('/', [$base_id, rawurlencode($table_name), $record_id]);
	if (!$raw) {
		$source_url .= AFV_AIRTABLE_FORMAT_STRING;
	}
	return airtable_request($source_url);
}

/**
 * 
 */
function airtable_update_record($base_id, $table_name, $payload, $typecast = true) {
	$dest_url = join('/', [$base_id, rawurlencode($table_name)]);
	return airtable_request($dest_url, 'POST', [
		'fields' => $payload,
		'typecast' => $typecast
	]);
}

/**
 * Returns multiple results.
 * 
 * @param $base_id
 * @param $table_name
 * @param $search_fields Field Name => search value
 * @param $join pass 'AND' or 'OR'
 * @param $throw_on_error_response bool
 */
function airtable_find_records($base_id, $table_name, $search_fields = [], $join = 'AND', $throw_on_error_response = true) {

	$result = [];

	$search_formula = '';
	if ($search_fields) {
		$filter_clauses = [];
		foreach ($search_fields as $name => $value) {
			$filter_clauses[] = '{'. $name .'}="'. $value .'"';
		}
		$search_formula = '?filterByFormula='. rawurlencode($join . '(' . join(',', $filter_clauses) . ')');
	}

	$url = join('/', [$base_id, rawurlencode($table_name), $search_formula]);
	$response = airtable_request($url, 'GET', [], $throw_on_error_response);

	if (isset($response['records'])) {
		return $response['records'];
	} else {
		if ($throw_on_error_response) {
			throw new Exception("Found ". count($response['records']) ." records in $table_name.");
		}
	}

	return $result;

}

/**
 * Expects a single result. Throws or return false if not exactly one record found.
 * 
 * @param $base_id
 * @param $table_name
 * @param $field_name Single field name or array of field names (OR'd) to search
 * @param $search_value Exact match.
 * @param $return Either 'id' or 'fields'.
 */
function airtable_find_single_record_by_field($base_id, $table_name, $field_name, $search_value, $return = 'id', $throw_on_error_response = true) {

	$result = false;

	if (!is_array($field_name)) {
		$field_name = [$field_name];
	}

	$filter_clauses = [];
	foreach ($field_name as $name) {
		$filter_clauses[] = '{'. $name .'}="'. $search_value .'"';
	}
	$formula = rawurlencode('OR(' . join(',', $filter_clauses) . ')');
	$url = join('/', [$base_id, rawurlencode($table_name), '?filterByFormula='. $formula]);

	$response = airtable_request($url, 'GET', [], $throw_on_error_response);

	if (isset($response['records'])) {
		if (count($response['records']) == 1) {
			return $response['records'][0][$return]; 
		} else {
			if ($throw_on_error_response) {
				throw new Exception("Found ". count($response['records']) ." records in $table_name matching '$search_value'.");
			} else {
				return false;
			}
		}
	}

	return $result;

}

function airtable_is_field_writeable($field_type) {
	return !in_array($field_type, ['autoNumber', 'button', 'count', 'formula', 'rollup']);
}

/**
 * 
 */
function airtable_load_bases() {
	return airtable_request('meta/bases');
}

/**
 * 
 */
function airtable_load_base_list() {
	$result = [];
	$meta = airtable_load_bases();
	if (isset($meta['bases'])) {
		foreach ($meta['bases'] as $base) {
			$result[$base['id']] = $base['name'];
		}
	}
	return $result;
}

/**
 * Load schema for individual table.
 */
function airtable_load_table_schema($base_id, $table_name) {

	$result = [];

	$schema = airtable_request("meta/bases/$base_id/tables/");
	foreach ($schema['tables'] as $table) {
		if ($table['name'] == $table_name) {
			foreach ($table['fields'] as $field) {
				$result[$field['name']] = $field;
			}
			return $result;
		}
	}

	return $result;

}

/**
 * Load schema for all tables
 */
function airtable_load_base_schema($base_id) {

	$result = [];

	$schema = airtable_request("meta/bases/$base_id/tables/");
	foreach ($schema['tables'] as $table) {
		foreach ($table['fields'] as $field) {
			$result[$table['name']][$field['name']] = $field;
		}
	}

	return $result;

}

/**
 * 
 */
function afv_load_remote_form($table, $record_id, $field_name_filters = []) {

	$forms_base = get_option('options_forms_base');
	$url = join('/', [$forms_base, rawurlencode($table), $record_id, AFV_AIRTABLE_FORMAT_STRING]);
	$result = airtable_request($url);

	$data = [];
	foreach ($result['fields'] as $key => $value) {

		$allowed = true;
		if ($field_name_filters) {
			foreach ($field_name_filters as $filter) {
				if (false !== strpos($key, $filter)) {
					$allowed = false;
					break;
				}
			}
		}
		
		if ($allowed) {
			$data[] = [
				'name' => $key,
				'text' => $value
			];
		}

	}

	return $data;

}

/**
 * 
 */
function airtable_request($request, $method='GET', $data=[], $throw_on_error_response = true) {

	$settings = AirtableSettings::load();

	if ($settings->airtable_api_key) {

		$api_key = $settings->airtable_api_key;
		$url = AFV_AIRTABLE_API_ROOT . $request;
		$ch = curl_init($url);

		$headers = ["Authorization: Bearer $api_key"];
		if ($data) {
			$headers[] = "Content-Type: application/json";
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		}

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($ch); 
		curl_close($ch);

		$result = json_decode($response, true);

		if ($throw_on_error_response && isset($result['error'])) {
			throw new Exception(isset($result['error']['type']) ? $result['error']['type'] : $result['error']);
		}
	
		return $result;

	}

}