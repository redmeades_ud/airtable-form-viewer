<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

add_action('afv_sync_forms', 'afv_sync_forms');
function afv_sync_forms() {

    $settings = AirtableSettings::load();

    try {

        //find published forms
        $published_forms_from_airtable = airtable_find_records($settings->forms_base, 'Workflow Form List', ['Publish' => true]);
        $airtable_form_names = array_map(function ($entry) {
            return $entry['fields']['Form Name'];
        }, $published_forms_from_airtable);

        //find current CPT forms
        $published_forms_local = get_posts([
            'post_type' => AFV_POST_TYPE_FORM,
            'post_status' => 'publish'
        ]);
        $local_form_names = array_map(function ($entry) {
            return $entry->post_title;
        }, $published_forms_local);

        //publish remote forms that do not exist locally
        $to_publish = array_diff($airtable_form_names, $local_form_names);
        foreach ($to_publish as $form_name) {
            wp_insert_post([
                'post_type' => AFV_POST_TYPE_FORM,
                'post_status' => 'publish',
                'post_title' => $form_name,
                'post_content' => "[airtable_form_view form='$form_name']"
            ]);
        }

        //delete local forms that do not exist remotely
        $to_delete = array_diff($local_form_names, $airtable_form_names);
        foreach ($to_delete as $form_name) {
            if ($post = get_page_by_title($form_name, OBJECT, AFV_POST_TYPE_FORM)) {
                wp_delete_post($post);
            }
        }
        
    } catch (Exception $e) {
        error_log("Error syncing form posts: ". $e->getMessage());
    }

}