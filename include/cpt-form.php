<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define('AFV_POST_TYPE_FORM', 'form');

add_action('init', function () {

	$labels = [
		"name" => __( "Forms", AFV_PLUGIN_ID ),
		"singular_name" => __( "Form", AFV_PLUGIN_ID ),
	];

	$args = [
		"label" => __( "Forms", AFV_PLUGIN_ID ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => false,
		"delete_with_user" => false,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => AFV_POST_TYPE_FORM, "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "excerpt", "custom-fields" ],
	];

	register_post_type( AFV_POST_TYPE_FORM, $args );

});