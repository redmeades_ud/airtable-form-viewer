<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * 
 */
add_action('wp_ajax_afv_load_form_ajax', 'afv_load_form_ajax');
add_action('wp_ajax_nopriv_afv_load_form_ajax', 'afv_load_form_ajax');
function afv_load_form_ajax() {

    check_ajax_referer(AFV_NONCE_KEY);
    afv_ajax_check_settings();

    try {

        $form = afv_load_remote_form($_REQUEST['formName'], $_REQUEST['formRecordId'], ['_']);

        wp_send_json_success([
            'form' => $form
        ]);

    } catch (Exception $e) {
        
        wp_send_json_error([
            'error' => $e->getMessage()
        ]);

    }

}

/**
 * 
 */
add_action('wp_ajax_afv_save_response_ajax', 'afv_save_response_ajax');
add_action('wp_ajax_nopriv_afv_save_response_ajax', 'afv_save_response_ajax');
function afv_save_response_ajax() {

    check_ajax_referer(AFV_NONCE_KEY);

    $settings = AirtableSettings::load();
    afv_ajax_check_settings($settings);
    
    try {
        
        $payload = $_REQUEST;
        unset($payload['action']);
        unset($payload['_ajax_nonce']);

        $url = $settings->workflow_submission_webhook;
        $response = wp_remote_post($url, [
            'body' => $payload,
            'timeout' => 30
        ]);

        if (is_wp_error($response)) {
            throw new Exception($response->get_error_message());
        }

        //if the webhook is off it will return 'Accepted'
        if ($response['body'] == 'Accepted') {
            $response['body'] = json_encode($payload + [
                'result' => 'QUEUED',
                'resultCode' => 'QUEUED',
                'resultText' => 'Your request has been queued in the system, but not processed yet.',
            ]);
        }

        wp_send_json_success([
            'response' => $response
        ]);

    } catch (Exception $e) {
        
        wp_send_json_error([
            'error' => $e->getMessage()
        ]);
        
    }

}


/**
 * 
 */
function afv_ajax_check_settings($settings = null) {
    $settings = $settings ?? AirtableSettings::load();
    if (!$settings->valid()) {
        wp_send_json_error([
            'error' => 'The system is not set up yet. Please contact your systems administrator.'
        ]);
    }
}