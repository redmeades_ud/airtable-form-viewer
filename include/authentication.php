<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

add_filter('authenticate', 'afv_airtable_authenticate', 10, 3 );
function afv_airtable_authenticate($user, $username, $password) {

    if ($user) {
        return $user;
    }

    if (!$username || !$password) {
        return new WP_Error('denied', 'Please enter your username and password.');;
    }

    //lookup record in airtable
    $settings = AirtableSettings::load();
        
    //find airtable record
    $contact = airtable_find_single_record_by_field(
        $settings->authentication_base, 
        $settings->authentication_table, 
        [$settings->username_field, 'StaffID'],
        $username, 
        'fields', 
        false
    );

    if ($contact) {

        //check password
        if ($password == $contact[$settings->password_field]) {

            //airtable login ok, find or create local user
            if (!$user = get_user_by('login', $username)) {

                $user_id = wp_insert_user([
                    'user_email' => $contact['Primary Email'],
                    'user_login' => $contact['Primary Email'],
                    'first_name' => $contact['First Name'],
                    'last_name' => $contact['Last Name'],
                ]);

                update_user_meta($user_id, AFV_USER_META_STAFF_ID, $contact['StaffID']);

                $user = new WP_User($user_id);

            }

        } 

    }

    return $user;

}

function demo_auth( $user, $username, $password ){
    // Make sure a username and password are present for us to work with
    if($username == '' || $password == '') return;

    $response = wp_remote_get( "http://localhost/auth_serv.php?user=$username&pass=$password" );
    $ext_auth = json_decode( $response['body'], true );

     if( $ext_auth['result']  == 0 ) {
        // User does not exist,  send back an error message
        $user = new WP_Error( 'denied', __("ERROR: User/pass bad") );

     } else if( $ext_auth['result'] == 1 ) {
         // External user exists, try to load the user info from the WordPress user table
         $userobj = new WP_User();
         $user = $userobj->get_data_by( 'email', $ext_auth['email'] ); // Does not return a WP_User object :(
         $user = new WP_User($user->ID); // Attempt to load up the user with that ID

         if( $user->ID == 0 ) {
             // The user does not currently exist in the WordPress user table.
             // You have arrived at a fork in the road, choose your destiny wisely

             // If you do not want to add new users to WordPress if they do not
             // already exist uncomment the following line and remove the user creation code
             //$user = new WP_Error( 'denied', __("ERROR: Not a valid user for this system") );

             // Setup the minimum required user information for this example
             $userdata = array( 'user_email' => $ext_auth['email'],
                                'user_login' => $ext_auth['email'],
                                'first_name' => $ext_auth['first_name'],
                                'last_name' => $ext_auth['last_name']
                                );
             $new_user_id = wp_insert_user( $userdata ); // A new user has been created

             // Load the new user info
             $user = new WP_User ($new_user_id);
         } 

     }

     // Comment this line if you wish to fall back on WordPress authentication
     // Useful for times when the external service is offline
     remove_action('authenticate', 'wp_authenticate_username_password', 20);

     return $user;
}