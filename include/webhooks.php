<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

add_action('init', function() { 

    if ('/airtable-workflow-webhook' == strtok($_SERVER['REQUEST_URI'], '?')) {

        afv_process_webhook_workflow();
  
    } else if ('/airtable-refresh-forms' == strtok($_SERVER['REQUEST_URI'], '?')) {

        afv_process_webhook_refresh_forms();

    }
    
}, 5);

/**
 * 
 */
function afv_process_webhook_workflow() {

    try {

        if (afv_authenticate_webhook()) {

            airtable_copy_record(
                get_option('options_forms_base'), $_REQUEST['sourceTable'], $_REQUEST['sourceRecordId'],
                $_REQUEST['targetBaseId'], $_REQUEST['targetTable']
            );

            wp_send_json_success();

        } else {

            wp_send_json_error(['message' => 'Unauthorized'], 401);

        }

    } catch (Exception $e) {

        wp_send_json_error(['message' => $e->getMessage()], 501);

    }

}

/**
 * 
 */
function afv_process_webhook_refresh_forms() {

    try {

        if (afv_authenticate_webhook()) {

            afv_sync_forms();

            wp_send_json_success();

        } else {

            wp_send_json_error(['message' => 'Unauthorized'], 401);

        }

    } catch (Exception $e) {

        wp_send_json_error(['message' => $e->getMessage()], 501);

    }

}

/**
 * 
 */
function afv_authenticate_webhook() {
    
    $settings = AirtableSettings::load();

    //check basic auth and api key
    if (is_wp_error( wp_authenticate($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) )) {
        return false;
    }

    if ($_SERVER['HTTP_X_API_KEY'] != $settings->incoming_webhook_secret) {
        return false;
    }

    //is authenticated?
    return true;

}