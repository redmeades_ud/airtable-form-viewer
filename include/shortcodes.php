<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Simple list of forms.
 */
add_shortcode('airtable_form_list', function($atts) {

    $forms = get_posts([
        'post_type' => AFV_POST_TYPE_FORM,
        'post_status' => 'publish'
    ]);

    //include directly to maintain variable scope
    $template = afv_template('form-list-shortcode.php', true);
    require_once($template);

});

/**
 * For filling out a form
 */
add_shortcode('airtable_form_view', function($atts) {

    //require login to enter forms
    if (!is_user_logged_in()) {
        afv_template('login-required.php');
        return;
    }

    //which form has been requested?
    $atts = shortcode_atts([
        'form' => get_the_title()
    ], $atts);
    $form_name = $atts['form'];

    try {

        //get current user's StaffID
        $staff_id = get_user_meta(get_current_user_id(), AFV_USER_META_STAFF_ID, true);

        //load form record
        $form_record = airtable_load_form_settings($form_name);

        //get the actual form id
        $url = $form_record['miniExtensions URL'];
        $form_id = substr($url, strrpos($url, '/') + 1);
        $updated_form_id = $form_id . rawurlencode('?prefill_StaffID=' . $staff_id);

        //add user id to embedded url
        $embed_code = $form_record['Embed Code'];
        $position = strrpos($embed_code, $form_id);
        $authenticated_embed_code = substr_replace($embed_code, $updated_form_id, $position, strlen($form_id));

        return $authenticated_embed_code;

    } catch (Exception $e) {
        error_log('airtable_form_view: ' . $e->getMessage());
        return "A problem occurred retrieving this form. Please contact your system administrator.";
    }

});

/**
 * For form approvers
 */
add_shortcode('airtable_form_approve', function($atts) {

    //Integromat freaks out if this shortcode runs during post creation
    if (is_admin() || is_rest_api_request()) {
        return;
    }

    $nonce = wp_create_nonce(AFV_NONCE_KEY);
    $input_json = $atts['data'];

    //simple security check
    $data = json_decode($input_json);
    if ($_REQUEST['accessCode'] == $data->accessCode) {
        require_once(AFV_PLUGIN_DIR . 'templates/vue-component.php');
    } else {
        require_once(AFV_PLUGIN_DIR . 'templates/security-notice.php');
    }

});