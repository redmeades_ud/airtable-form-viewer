<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * https://wordpress.stackexchange.com/questions/221202/does-something-like-is-rest-exist
 */
function is_rest_api_request() {
    if ( empty( $_SERVER['REQUEST_URI'] ) ) {
        // Probably a CLI request
        return false;
    }

    $rest_prefix         = trailingslashit( rest_get_url_prefix() );
    $is_rest_api_request = strpos( $_SERVER['REQUEST_URI'], $rest_prefix ) !== false;

    return apply_filters( 'is_rest_api_request', $is_rest_api_request );
}

/**
 * 
 */
function afv_template($template_name, $return_filepath = false) {
    
    $found_template = false;
    $theme_template = get_theme_file_path('/'. AFV_PLUGIN_ID .'/' . $template_name);
    
    if (file_exists($theme_template)) {
        $found_template = $theme_template;
    } else {
        $found_template = AFV_PLUGIN_DIR . 'templates/'. $template_name;
    }

    if ($return_filepath) {
        return $found_template;
    } else {
        require_once($found_template);
    }

}

class AirtableSettings {
    
    private static $_instance = null;

    public $airtable_api_key = null;
    public $forms_base = null;
    public $workflow_submission_webhook = null;
    public $incoming_webhook_secret = null;
    public $authentication_base = null;
    public $authentication_table = null;
    public $username_field = null;
    public $password_field = null;

    private function __construct() {
        foreach (array_keys(get_object_vars($this)) as $key) {
            $this->{$key} = get_option("options_{$key}");
        }
    }

    public function valid() {
        foreach (array_keys(get_object_vars($this)) as $key) {
            if (!$this->{$key}) {
                return false;
            }
        }
        return true;
    }

    public static function load() {
        if (null == self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

}