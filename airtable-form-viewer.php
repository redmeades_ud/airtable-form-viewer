<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Plugin Name: Airtable Form Viewer
 * Description: Enable Airtable form workflow for University of Divinity.
 * Author: Tyson Lloyd
 * Author URI: https://tysonlloydconsulting.com
 * Version: 1.0.2
 * GitHub Plugin URI: https://github.com/tysonlt/airtable-form-viewer
 */

define('AFV_PLUGIN_ID', 'airtable-form-viewer');
define('AFV_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('AFV_PLUGIN_URL', plugin_dir_url(__FILE__));

define('AFV_AIRTABLE_API_ROOT', 'https://api.airtable.com/v0/');
define('AFV_NONCE_KEY', 'afv_nonce');
define('AFV_USER_META_STAFF_ID', 'airtable_staff_id');

foreach(glob(AFV_PLUGIN_DIR . 'include/*.php') as $file) {
    require_once($file);
}

add_action('init', function() {
    if (!wp_next_scheduled('afv_sync_forms')) {
        wp_schedule_event(time(), 'hourly', 'afv_sync_forms');
    }
});

register_deactivation_hook( __FILE__, 'afv_deactivate' ); 
function afv_deactivate() {
    wp_unschedule_event( wp_next_scheduled('afv_sync_forms'), 'afv_sync_forms' );
}

/**
 * TODO: clear out old posts for finished flows
 */

